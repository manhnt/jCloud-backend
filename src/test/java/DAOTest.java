import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import org.junit.jupiter.api.Test;

public class DAOTest {
    static String token = null;

    @Test
    void addUser() {
      DAO db = new DAO();
      assertEquals(db.addUser("test", "test"), true);
    }

    @Test
    void userLogin() {
        DAO db = new DAO();
        token = db.userLogin("test", "test");
        assertNotEquals(token, null);
    }

    @Test
    void userLogout() {
        DAO db = new DAO();
        assertEquals(db.userLogout(token), true);
    }
}
