import java.sql.*;
import java.util.HashMap;

public class DAO {
    private Connection conn = null;
    private ResultSet result = null;
    private PreparedStatement stmt = null;
    private String sql = null;

    public DAO() {
        try {
            conn = DriverManager.getConnection(
                "jdbc:mariadb://localhost:3306/jcloud?user=jcloud&password=jcloud");
        } catch (SQLException e) {
            System.out.println(e.toString());
        }
    }

    private void createStatement(String sql) throws SQLException {
        if (stmt != null) stmt.close();
        this.sql = sql;
        stmt = conn.prepareStatement(sql);
    }

    private void query() throws SQLException {
        stmt.executeUpdate();
        result = stmt.getResultSet();
    }

    public boolean addUser(String username, String password) {
        try {
            createStatement("INSERT INTO users(username, password) VALUES(?, ?)");
            stmt.setString(1, username);
            stmt.setString(2, password);
            query();
        } catch (SQLException e) {
            System.out.println(e.toString());
            return false;
        }
        return true;
    }

    public String userLogin(String username, String password) {
        try {
            createStatement("SELECT * FROM users WHERE username = ? AND password = ?");
            stmt.setString(1, username);
            stmt.setString(2, password);
            query();
            if (result.next()) {
                int uid = result.getInt("id");
                createStatement("INSERT INTO sessions(user, token) VALUES(?, UUID())");
                stmt.setInt(1, uid);
                query();
                createStatement("SELECT token from sessions WHERE user= ?");
                stmt.setInt(1, uid);
                query();
                result.next();
                return result.getString("token");
            }
            else return null;
        } catch (SQLException e) {
            System.out.println(e.toString());
            return null;
        }
    }

    public boolean userLogout(String token) {
        try {
            createStatement("DELETE FROM sessions WHERE token = ?");
            stmt.setString(1, token);
            query();
        } catch (SQLException e) {
            System.out.println(e.toString());
            return false;
        }
        return true;
    }

    public boolean userData(String username) {
        try {
            query();
        } catch (SQLException e) {
            System.out.println(e.toString());
            return false;
        }
        return true;
    }

    public int getUser(String token) {
         try {
            createStatement("SELECT user FROM sessions WHERE token = ?");
            stmt.setString(1, token);
            query();
            if (result.next())
                return result.getInt("user");
            else
                return 0;
        } catch (SQLException e) {
            System.out.println(e.toString());
            return 0;
        }
    }

    public boolean addFile(int user, String path) {
        try {
            createStatement("INSERT INTO files(user, path) VALUES(?, ?)");
            stmt.setInt(1, user);
            stmt.setString(2, path);
            query();
            return true;
        } catch (SQLException e) {
            System.out.println(e.toString());
            return false;
        }
    }

    public boolean verifySession(String token) {
        if (getUser(token) == 0)
            return false;
        else
            return true;
    }

    public HashMap<Integer, String> listFiles(int user) {
        HashMap<Integer, String> files = new HashMap<>();
        try {
            createStatement("SELECT id, path FROM files WHERE user = ?");
            stmt.setInt(1, user);
            query();
            while (result.next()) {
                files.put(result.getInt("id"), result.getString("path"));
            }
        } catch (SQLException e) {
            System.out.println(e.toString());
        }
        return files;
    }
}
