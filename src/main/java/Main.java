import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import io.javalin.Javalin;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

public class Main {
    private static final DAO db = new DAO();

    public static void startServer() {
        Javalin app = Javalin.create()
            .port(7000)
            .enableStaticFiles("/public/upload")
            .enableCorsForOrigin("*")
            .start();

        app.post("/register", ctx -> {
            JsonObject request = new Gson().fromJson(ctx.body(), JsonObject.class);
            if (db.addUser(
                request.get("username").getAsString(),
                request.get("password").getAsString())
            ) ctx.result("ok");
            else ctx.result("error");
        });

        app.post("/login", ctx -> {
            JsonObject request = new Gson().fromJson(ctx.body(), JsonObject.class);
            String token = db.userLogin(
                request.get("username").getAsString(),
                request.get("password").getAsString());
            JsonObject reply = new JsonObject();
            if (token != null) reply.addProperty("token", token);
            ctx.result(reply.toString());
        });

        app.post("/logout", ctx -> {
            JsonObject request = new Gson().fromJson(ctx.body(), JsonObject.class);
            db.userLogout(request.get("token").getAsString());
        });

        app.post("/upload", ctx -> {
            ctx.uploadedFiles("files").forEach(file -> {
                try {
                    int user = db.getUser(ctx.header("token"));
                    if (user == 0) {
                        ctx.result("error");
                        return;
                    }
                    db.addFile(user, file.getName());
                    FileUtils.copyInputStreamToFile(
                        file.getContent(),
                        new File("target/classes/public/upload/" + file.getName())
                    );
                    ctx.result("ok");
                } catch (IOException e) {
                    ctx.result("error");
                    System.out.println(e.toString());
                }
            });
        });

        app.post("/files", ctx -> {
            HashMap<Integer, String> files = db.listFiles(db.getUser(ctx.header("token")));
            JsonArray reply = new JsonArray();
            for(HashMap.Entry<Integer, String> entry : files.entrySet()) {
                JsonObject file = new JsonObject();
                file.addProperty("id", entry.getKey().toString());
                file.addProperty("path", entry.getValue());
                reply.add(file);
            }
            ctx.result(reply.toString());
        });
    }

    public static void main(String[] args) {
        startServer();
    }
}
