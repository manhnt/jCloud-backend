DROP DATABASE jcloud;
CREATE DATABASE jcloud;
USE jcloud;

CREATE TABLE users(
  id int PRIMARY KEY AUTO_INCREMENT,
  username nvarchar(50) NOT NULL UNIQUE,
  password nvarchar(50) NOT NULL
);

CREATE TABLE sessions(
  id int PRIMARY KEY AUTO_INCREMENT,
  user int NOT NULL UNIQUE,
  token nvarchar(36) NOT NULL
);

CREATE TABLE files(
  id int PRIMARY KEY AUTO_INCREMENT,
  user int NOT NULL,
  path nvarchar(50) NOT NULL UNIQUE
);

